module chainmaker.org/chainmaker/vm-engine/v2/vm_mgr

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.1
	chainmaker.org/chainmaker/pb-go/v2 v2.3.2
	chainmaker.org/chainmaker/protocol/v2 v2.3.2
	chainmaker.org/chainmaker/vm-docker-go/v2 v2.3.3
	github.com/emirpasic/gods v1.18.1
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/spf13/viper v1.15.0
	github.com/stretchr/testify v1.8.1
	go.uber.org/atomic v1.10.0
	go.uber.org/zap v1.24.0
	google.golang.org/grpc v1.53.0
)
